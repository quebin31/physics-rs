pub trait FloatComps {
  fn feq(self, other: Self, precision: u8) -> bool;
  fn flt(self, other: Self, precision: u8) -> bool;
  fn fgt(self, other: Self, precision: u8) -> bool;
  fn fle(self, other: Self, precision: u8) -> bool;
  fn fge(self, other: Self, precision: u8) -> bool;
}

impl FloatComps for f64 {
  fn feq(self, other: Self, precision: u8) -> bool {
    let integer_part_self  =  self.floor() as isize;
    let integer_part_other =  other.floor() as isize;

    if integer_part_self != integer_part_other { return false; }

    let decimal_part_self = (self - integer_part_self as f64) * 10f64.powi(precision as i32);
    let decimal_part_other = (other - integer_part_other as f64) * 10f64.powi(precision as i32);

    let decimal_part_self = decimal_part_self.floor() as isize;
    let decimal_part_other = decimal_part_other.floor() as isize;

    decimal_part_self == decimal_part_other
  }

  fn flt(self, other: Self, precision: u8) -> bool {
    let integer_part_self  =  self.floor() as isize;
    let integer_part_other =  other.floor() as isize;

    if integer_part_self > integer_part_other { return false; }
    if integer_part_self < integer_part_other { return true; }

    let decimal_part_self = (self - integer_part_self as f64) * 10f64.powi(precision as i32);
    let decimal_part_other = (other - integer_part_other as f64) * 10f64.powi(precision as i32);

    let decimal_part_self = decimal_part_self.floor() as isize;
    let decimal_part_other = decimal_part_other.floor() as isize;

    decimal_part_self < decimal_part_other
  }

  fn fgt(self, other: Self, precision: u8) -> bool {
    let integer_part_self  =  self.floor() as isize;
    let integer_part_other =  other.floor() as isize;

    if integer_part_self < integer_part_other { return false; }
    if integer_part_self > integer_part_other { return true; }

    let decimal_part_self = (self - integer_part_self as f64) * 10f64.powi(precision as i32);
    let decimal_part_other = (other - integer_part_other as f64) * 10f64.powi(precision as i32);

    let decimal_part_self = decimal_part_self.floor() as isize;
    let decimal_part_other = decimal_part_other.floor() as isize;

    decimal_part_self > decimal_part_other
  }

  fn fle(self, other: Self, precision: u8) -> bool {
    self.flt(other, precision) || self.feq(other, precision)
  }

  fn fge(self, other: Self, precision: u8) -> bool {
    self.fgt(other, precision) || self.feq(other, precision)
  }
}

pub fn euler_method(a: f64, b: f64, delta: f64) -> f64 {
  a + b * delta
}

pub fn runge_kutta4<F>(x: f64, y: f64, h: f64, f: F) -> f64 where F: (Fn(f64, f64) -> f64) {
  let k1 = f(x,y);
  let k2 = f(x + h/2.0, y + h/2.0 * k1);
  let k3 = f(x + h/2.0, y + h/2.0 * k2);
  let k4 = f(x + h, y + h * k3);

  (k1 + 2.0 * (k2 + k3) + k4)/6.0
}
