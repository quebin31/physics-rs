use math;

pub struct PoblationData {
  pub time: f64,
  pub pa: f64,
  pub pb: f64
}

impl Clone for PoblationData {
  fn clone(&self) -> Self {
    PoblationData { time: self.time, pa: self.pa, pb: self.pb }
  }
}

pub fn dam_predator_with_euler(pa: f64, pb: f64, a: f64, b: f64, c: f64, d: f64, t0: f64, tf: f64, n: usize) -> Vec<PoblationData> {
  let mut all_data = Vec::new();
  let mut data = PoblationData {
    time: t0,
    pa: pa,
    pb: pb
  };

  let h  = (tf - t0) / n as f64;
  let dr = | pa: f64, pb: f64 | -> f64 {  a * pa - c * pa * pb };
  let df = | pa: f64, pb: f64 | -> f64 { -b * pb + d * pa * pb };

  while data.time <= tf {
    all_data.push(data.clone());

    let tpa = data.pa + h * dr(data.pa, data.pb);
    let tpb = data.pb + h * df(data.pa, data.pb);
    data.pa = tpa;
    data.pb = tpb;

    data.time += h;
  }

  all_data
}

pub fn dam_predator_with_rkutta(pa: f64, pb: f64, a: f64, b: f64, c: f64, d: f64, t0: f64, tf: f64, n: usize) -> Vec<PoblationData> {
  let mut all_data = Vec::new();
  let mut data = PoblationData {
    time: t0,
    pa: pa,
    pb: pb
  };

  let h  = (tf - t0) / n as f64;
  let dr = | pb: f64, pa: f64 | -> f64 {  a * pa - c * pa * pb };
  let df = | pa: f64, pb: f64 | -> f64 { -b * pb + d * pa * pb };

  while data.time <= tf {
    all_data.push(data.clone());

    let tpa = data.pa + h * math::runge_kutta4(data.pb, data.pa, h, dr);
    let tpb = data.pb + h * math::runge_kutta4(data.pa, data.pb, h, df);
    data.pa = tpa;
    data.pb = tpb;

    data.time += h;
  }

  all_data
}
